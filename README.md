# Scripts for Backup Processes

## Quick start

1. Install all necessary modules
```
$ cd PATH/TO/PROJECT
$ pip install -r requirements.txt
```

2. Copy sample dotenv file and put your data

```
$ cp sample.env .env
```

dotenv file example:
```
#telegram token and channel_id
tel_token=''
telegram_id=

#twitter data
key=''
secret_key=''
twi_token=''
token_secret=''

```

3. Run script
```
$ python <script>.py
```
