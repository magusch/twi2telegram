import tweepy
import sqlite3
import telebot

import os,time

#load env
from dotenv import load_dotenv
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

#telegram data
telegram_token=os.environ.get('tel_token') #token for bot 
user_id=os.environ.get('telegram_id') #telegram channel id for sending message

#twi_data https://developer.twitter.com/en/apps  
consumer_key=os.environ.get('key')
consumer_secret=os.environ.get('secret_key')
access_token=os.environ.get('twi_token')
access_token_secret=os.environ.get('token_secret')

#authentification in twitter
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth)

#Database
conn = sqlite3.connect("mydatabase.db")
cursor = conn.cursor()

#Telegram
bot = telebot.TeleBot(telegram_token)


def update_last_tweet(last_tweet_id):
	try:
		cursor.execute("""UPDATE tweets SET last_id=%s""" %(str(last_tweet_id)))
		conn.commit()
		print('tweet_id saved')
	except Exception as e:
		print('Error:')
		print(e)


def load_last_tweet():
	try:
		cursor.execute("SELECT last_id FROM tweets")
		last_tweet_id=cursor.fetchone()[0]
		return last_tweet_id
	except Exception as e:
		if str(e).find('no such table')!=-1:
			cursor.execute("CREATE TABLE tweets (last_id int)")
			cursor.execute("INSERT INTO tweets VALUES (1)")
			conn.commit()
		else:
			print('Error in loading:')
			print(e)



def tweets():
	last_tweet_id=1#load_last_tweet()
	print(last_tweet_id)
	try:
		user_tweets = api.user_timeline(since_id=last_tweet_id,count=1,tweet_mode='extended')
		last_tweet_id=user_tweets[0].id
	except:
		print('Not new tweets')
		return

	for tweet in user_tweets:
		if tweet.full_text[:1]=='@':
			print('mention')
			next
		elif tweet.full_text[:2]=='RT':
			print(tweet.retweeted_status._json['full_text'])
		bot.send_message(user_id, tweet.full_text)
	update_last_tweet(last_tweet_id) #save last tweet_id


if __name__ == '__main__':
	while True:
		tweets()
		time.sleep(30*60)